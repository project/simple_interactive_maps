Drupal.simple_interactive_maps.navigate_action = function (elem, config, region) {
  elem.addEventListener('click', function (e) {
    // Open a new tab and navigate to the URL
    if (config.new_tab) {
      window.open(config.url);
    } else {
      window.location.href = config.url;
    }
  });
  // Add an ariarole of link to the element.
  elem.setAttribute('role', 'link');
};
