import MapEmbedEditing from './mapEmbedEditing';
import MapEmbedToolbar from './mapEmbedToolbar';
import MapEmbedUi from './mapEmbedUi';
import { Plugin } from 'ckeditor5/src/core';

class MapEmbed extends Plugin {
  static get requires() {
    return [
      MapEmbedEditing,
      MapEmbedUi,
      MapEmbedToolbar
    ];
  }

  static get pluginName() {
    return 'MapEmbed';
  }
}

export default {
  MapEmbed,
};
