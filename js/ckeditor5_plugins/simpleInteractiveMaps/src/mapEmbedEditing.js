import { Plugin } from 'ckeditor5/src/core';
import { toWidget, toWidgetEditable } from 'ckeditor5/src/widget';
import { Widget } from 'ckeditor5/src/widget';
import InsertMapEmbedCommand from './InsertMapEmbedCommand';

export default class MapEmbedEditing extends Plugin {
  static get requires() {
    return [Widget];
  }

  init() {
    this._defineSchema();
    this._defineConverters();
    this.editor.commands.add(
      'insertMapEmbed',
      new InsertMapEmbedCommand(this.editor),
    )
  }

  _defineSchema() {
    const schema = this.editor.model.schema;

    schema.register('simpleMap', {
      isObject: true,
      allowWhere: '$block',
      allowAttributes: ['mapId', 'mapTitle', 'showDescription'],
      isBlock: true
    });

  }

  _defineConverters() {
    const {conversion} = this.editor;

    conversion.for('upcast').elementToElement({
      view: {
        name: 'simple-map',
        attributes: {
          'data-map-id': true,
          'data-map-title': true,
          'data-show-description': true
        }
      },
      model: (viewElement, { writer: modelWriter }) => {
        return modelWriter.createElement('simpleMap', {
          mapId: viewElement.getAttribute('data-map-id'),
          mapTitle: viewElement.getAttribute('data-map-title'),
          showDescription: viewElement.getAttribute('data-show-description')
        });
      }
    });

    conversion.for('dataDowncast').elementToElement({
      model: 'simpleMap',
      view: (modelElement, { writer: viewWriter }) => {
        const mapId = modelElement.getAttribute('mapId');
        const mapTitle = modelElement.getAttribute('mapTitle');
        const showDescription = modelElement.getAttribute('showDescription');
        return viewWriter.createContainerElement('simple-map', {
          'data-map-id': mapId,
          'data-map-title': mapTitle,
          'data-show-description': showDescription
        });
      }
    });

    conversion.for('editingDowncast').elementToElement({
      model: 'simpleMap',
      view: (modelElement, { writer: viewWriter }) => {
        const mapId = modelElement.getAttribute('mapId');
        const mapTitle = modelElement.getAttribute('mapTitle');
        const showDescription = modelElement.getAttribute('showDescription');

        const mapDiv = viewWriter.createContainerElement('div', {
          class: 'interactive_map__edit_wrapper',
          'data-map-id': mapId,
          'data-map-title': mapTitle,
          'data-show-description': showDescription
        });

        const placeholderSpan = viewWriter.createUIElement('div', {
          class: 'map-placeholder__text'
        }, function (domDocument) {
          const domElement = this.toDomElement(domDocument);
          let innerText = `Map: ${mapTitle} (${mapId})`;
          if (showDescription) {
            innerText += ' - with Description';
          }
          domElement.innerText = innerText;
          return domElement
        });

        // Create a image placeholder and insert it ahead of the placeholder span.
        const imagePlaceholder = viewWriter.createUIElement('img', {
          class: 'map-placeholder__image',
        }, function (domDocument) {
          const domElement = this.toDomElement(domDocument);
          domElement.src = `/admin/structure/interactive-map/${mapId}/thumbnail`;
          return domElement;
        });
        viewWriter.insert(viewWriter.createPositionAt(mapDiv, 1), imagePlaceholder);

        viewWriter.insert(viewWriter.createPositionAt(mapDiv, 0), placeholderSpan);

        return toWidget(mapDiv, viewWriter, {label: "interactive map widget"});
      }
    });

  }

}
