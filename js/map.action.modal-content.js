Drupal.simple_interactive_maps.modal_content = function (elem, config, region) {
  elem.addEventListener('click', function (e) {
    let mapDetailDialog = Drupal.dialog(`<div>${config.modal_content}</div>`, {
      title: region['label'],
      dialogClass: 'interactive-map-dialog',
      autoOpen: true,
      resizable: false,
      modal: true,
      width: 800,
      closeOnEscape: true,
    });
    // Open the dialog.
    mapDetailDialog.showModal();
  });

  // Add an aria button role to the element.
  elem.setAttribute('role', 'button');
}
