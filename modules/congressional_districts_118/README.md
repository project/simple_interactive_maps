# Maps of the Congressional Districts of the United States

These maps were created from the Shapefiles distributed by the U.S. Census Bureau. The Shapefiles were downloaded from the [Census Bureau's TIGER/Line Shapefiles](https://www.census.gov/geographies/mapping-files/time-series/geo/tiger-line-file.html) page. The Shapefiles were downloaded for the 118th Congress.
