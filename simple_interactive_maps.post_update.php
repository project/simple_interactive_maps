<?php

/**
 * @file
 * Post update hooks for simple_interactive_maps.
 */

use Drupal\simple_interactive_maps\InteractiveMapInterface;

/**
 * Implements hook_post_update_NAME().
 */

/**
 * Adds the new plugin for the base map to the configurations.
 */
function simple_interactive_maps_post_update_add_plugin(&$sandbox): void {
  $etm = Drupal::entityTypeManager();

  $mapStorage = $etm->getStorage('interactive_map');

  $maps = $mapStorage->loadMultiple();

  foreach ($maps as $index => $map) {
    assert($map instanceof InteractiveMapInterface);
    $map->set('base_map', 'us_states_territories');
    $map->save();
  }
}
