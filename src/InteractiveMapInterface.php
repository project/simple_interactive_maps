<?php

declare(strict_types=1);

namespace Drupal\simple_interactive_maps;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an interactive map entity type.
 */
interface InteractiveMapInterface extends ConfigEntityInterface {

  /**
   * Gets the embed code for the interactive map.
   *
   * @return string
   *   The embed code.
   */
  public function getEmbedCode(): string;

  /**
   * Recursively compare two copies of the group data.
   *
   * @param array $groups
   *   The groups data to compare with.
   *
   * @return bool
   *   Returns true if there are no differences.
   */
  public function compareGroupsData(array $groups): bool;

  /**
   * Loads the region data from the interactive map.
   *
   * @return array
   *   The region data.
   */
  public function getRegions(): array;

  /**
   * Sets the region data for the interactive map.
   *
   * @param array $regions
   *   The region data.
   */
  public function setRegions(array $regions): void;

  /**
   * Loads the group data from the interactive map.
   *
   * @return array
   *   The group data.
   */
  public function getGroups(): array;

  /**
   * Sets the group data for the interactive map.
   *
   * @param array $groups
   *   The group data.
   */
  public function setGroups(array $groups): void;

}
