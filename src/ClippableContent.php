<?php

declare(strict_types=1);

namespace Drupal\simple_interactive_maps;

/**
 * Provides a service for creating clippable content.
 */
final class ClippableContent {

  /**
   * Return a render array that will display the clippable content.
   *
   * @param string $clippableString
   *   String to be copied.
   * @param string $id
   *   Identifier used to target the copy link.
   * @param string $wrapperTag
   *   HTML tag to wrap the clippable content.
   *
   * @return array
   *   Render array.
   */
  public function createClippableItem(string $clippableString, string $id, string $wrapperTag = 'div'): array {
    return [
      '#type' => 'html_tag',
      '#tag' => $wrapperTag,
      '#value' => $clippableString,
      '#attributes' => [
        'class' => ['clippable-content__wrapper ' . $id . '__wrapper'],
      ],
      'copyLink' => [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => ' &#x2398;',
        '#attributes' => [
          'class' => ['clippable-content__link copy-link ' . $id],
          'data-clipboard-text' => $clippableString,
        ],
      ],
      '#attached' => [
        'library' => ['simple_interactive_maps/clippable_content'],
      ],
    ];
  }

}
