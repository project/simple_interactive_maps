<?php

declare(strict_types=1);

namespace Drupal\simple_interactive_maps;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Base class for map_action plugins.
 */
abstract class MapActionPluginBase extends PluginBase implements PluginFormInterface, MapActionInterface, ConfigurableInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function isSystem(): bool {
    return $this->pluginDefinition['is_system'];
  }

  /**
   * {@inheritdoc}
   */
  abstract public function getActionLibrary(): string;

  /**
   * {@inheritdoc}
   */
  abstract public function getActionConfiguration(): array;

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    // Merge the default configuration to fill in any missing values.
    $this->configuration = array_merge($this->defaultConfiguration(), $configuration);
  }

}
