<?php

declare(strict_types=1);

namespace Drupal\simple_interactive_maps\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\simple_interactive_maps\InteractiveMapInterface;
use Drupal\simple_interactive_maps\MapActionPluginManager;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Returns responses for Simple Interactive Maps routes.
 */
final class MapGroupController extends ControllerBase implements ContainerInjectionInterface {

  public function __construct(protected MapActionPluginManager $actionPluginManager) {}

  /**
   * Builds the response.
   */
  public function __invoke(?InteractiveMapInterface $interactive_map = NULL): array {
    if (!($interactive_map instanceof InteractiveMapInterface)) {
      throw new HttpException(404, 'Interactive map not found.');
    }

    $groups = $interactive_map->getGroups();

    \uasort($groups, static fn ($a, $b) => \strcasecmp($a['label'], $b['label']));

    $build = [];

    $build['add_group'] = [
      "#url" => Url::fromRoute(
          'simple_interactive_maps.map_group_add_form',
          ['interactive_map' => $interactive_map->id()],
          [
            'query' => [
              'destination' => Url::fromRoute('<current>')
                ->toString(),
            ],
          ],
      ),
      '#attributes' => [
        'class' => ['button'],
      ],
      '#title' => $this->t('Add group'),
      '#type' => 'link',
    ];

    $build['table'] = [
      '#empty' => $this->t('No groups found.'),
      '#header' => [
        'group' => $this->t('Group'),
        'operations' => $this->t('Operations'),
        'override_action' => $this->t('Override Action'),
        'override_colors' => $this->t('Override Colors'),
        'override_tooltip' => $this->t('Override Tooltip'),
      ],
      '#type' => 'table',
    ];

    foreach ($groups as $groupId => $group) {
      $build['table']['#rows'][$groupId] = [
        'data' => [
          'group' => [
            'data' => $group['label'],
          ],
          'operations' => [
            'data' => [
              '#links' => [
                'delete' => [
                  'title' => $this->t('Delete'),
                  'url' => Url::fromRoute('simple_interactive_maps.group_delete_confirm', [
                    'group' => $groupId,
                    'interactive_map' => $interactive_map->id(),
                  ], [
                    'query' => [
                      'destination' => Url::fromRoute('<current>')
                        ->toString(),
                    ],
                  ]),
                ],
                'edit' => [
                  'title' => $this->t('Edit'),
                  'url' => Url::fromRoute('simple_interactive_maps.map_group_edit_form', [
                    'group' => $groupId,
                    'interactive_map' => $interactive_map->id(),
                  ], [
                    'query' => [
                      'destination' => Url::fromRoute('<current>')
                        ->toString(),
                    ],
                  ]),
                ],
              ],
              '#type' => 'dropbutton',
            ],
          ],
          'override_action' => [
            'data' => $group['override_action'] ? $this->t('Yes') : $this->t('No'),
          ],
          'override_colors' => [
            'data' => $group['override_colors'] ? $this->t('Yes') : $this->t('No'),
          ],
          'override_tooltip' => [
            'data' => $group['override_tooltip'] ? $this->t('Yes') : $this->t('No'),
          ],
        ],
        'data-group-id' => $groupId,
      ];
    }

    return $build;
  }

}
