<?php

declare(strict_types=1);

namespace Drupal\simple_interactive_maps\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\simple_interactive_maps\InteractiveMapInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Returns responses for Simple Interactive Maps routes.
 */
final class AjaxMapLoaderController extends ControllerBase {

  /**
   * The map storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private EntityStorageInterface $mapStorage;

  /**
   * The controller constructor.
   */
  public function __construct(
    private Renderer $renderer,
    EntityTypeManagerInterface $entityTypeManager,
  ) {
    $this->mapStorage = $entityTypeManager->getStorage('interactive_map');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('renderer'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke(Request $request): AjaxResponse {

    // Load the interactive_map from the POST parameter map_to_load.
    $map_id = $request->request->get('map_to_load');
    $target_element = $request->request->get('target_element');

    $interactive_map = $this->mapStorage->load($map_id);

    if (!($interactive_map instanceof InteractiveMapInterface)) {
      throw new HttpException(404, 'Interactive map not found.');
    }

    $build['map'] = [
      '#theme' => 'interactive_map',
      '#map' => $interactive_map,
    ];

    $rendered_map = $this->renderer->render($build);

    $response = new AjaxResponse();
    $ajax_command = new ReplaceCommand('#' . $target_element, (string) $rendered_map);
    $response->addCommand($ajax_command);
    return $response;
  }

}
