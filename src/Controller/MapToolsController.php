<?php

declare(strict_types=1);

namespace Drupal\simple_interactive_maps\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\simple_interactive_maps\InteractiveMapInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Returns responses for Simple Interactive Maps routes.
 */
final class MapToolsController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function __invoke(InteractiveMapInterface $interactive_map = NULL): array {

    if (!($interactive_map instanceof InteractiveMapInterface)) {
      throw new HttpException(404, 'Interactive map not found');
    }

    $build['content'] = [
      '#theme' => 'interactive_map_tools',
      '#map' => $interactive_map,
    ];

    return $build;
  }

}
