<?php

namespace Drupal\simple_interactive_maps;

use Drupal\Core\Url;

/**
 * Provides a trait for generating a table of groups.
 */
trait GroupTableGeneratorTrait {

  /**
   * Generate a table of groups.
   *
   * @param array $groups
   *   An array of groups.
   * @param \Drupal\simple_interactive_maps\InteractiveMapInterface $map
   *   The map the groups belong to.
   *
   * @return array
   *   A render array for a table of groups.
   */
  public function generateGroupsTable(array $groups, InteractiveMapInterface $map): array {
    $table = [
      '#type' => 'table',
      '#header' => [
        $this->t('Group Name'),
        $this->t('Group ID'),
        $this->t('Overrides Colors'),
        $this->t('Overrides Action'),
        $this->t('Overrides Tooltip'),
        $this->t('Edit'),
      ],
      '#empty' => 'No groups created.',
      '#attributes' => [
        'id' => 'groups-table',
      ],
    ];

    foreach ($groups as $id => $group) {
      $table[$id] = [
        '#attributes' => [
          'class' => ['group-form-row'],
          'id' => ['row-group-' . strtolower($id)],
        ],
      ];

      $table[$id]['label'] = [
        '#type' => 'item',
        '#markup' => $group['label'],
      ];

      $table[$id]['id'] = [
        '#type' => 'item',
        '#markup' => $id,
      ];

      // Override Colors.
      $table[$id]['override_colors'] = [
        '#type' => 'item',
        '#markup' => $group['override_colors'] ? 'Yes' : 'No',
        '#attributes' => [
          'class' => ['group-override-colors'],
        ],
      ];

      // Override Action.
      $table[$id]['override_action'] = [
        '#type' => 'item',
        '#markup' => $group['override_action'] ? 'Yes' : 'No',
        '#attributes' => [
          'class' => ['group-override-action'],
        ],
      ];

      // Override Tooltip.
      $table[$id]['override_tooltip'] = [
        '#type' => 'item',
        '#markup' => $group['override_tooltip'] ? 'Yes' : 'No',
        '#attributes' => [
          'class' => ['group-override-tooltip'],
        ],
      ];

      // Create drop button with edit/delete links.
      $table[$id]['actions'] = [
        '#type' => 'dropbutton',
        '#links' => [
          'edit' => [
            'title' => $this->t('Edit'),
            'url' => Url::fromRoute('simple_interactive_maps.map_group_edit_form', [
              'interactive_map' => $map->id(),
              'group' => $id,
            ]),
            'attributes' => [
              'class' => ['use-ajax'],
              'data-dialog-type' => 'modal',
              'data-dialog-options' => \json_encode([
                'width' => 1000,
                'minHeight' => 600,
              ]),
            ],
          ],
          'delete' => [
            'title' => $this->t('Delete'),
            'url' => Url::fromRoute('simple_interactive_maps.group_delete_confirm', [
              'interactive_map' => $map->id(),
              'group' => $id,
            ]),
            'attributes' => [
              'class' => ['use-ajax'],
              'data-dialog-type' => 'modal',
              'data-dialog-options' => \json_encode([
                'width' => 400,
                'minHeight' => 200,
              ]),
            ],
          ],
        ],
      ];

    }

    return $table;
  }

}
