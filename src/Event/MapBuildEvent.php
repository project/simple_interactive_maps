<?php

namespace Drupal\simple_interactive_maps\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\simple_interactive_maps\InteractiveMapInterface;
use Drupal\simple_interactive_maps\MapRenderHelper;

/**
 * Event that is fired when a map is being built.
 */
class MapBuildEvent extends Event {

  /**
   * Event name.
   */
  public const EVENT_NAME = 'simple_interactive_maps.map_build';

  public function __construct(
    private InteractiveMapInterface $interactiveMap,
    private MapRenderHelper $renderData,
    private string $context = '',
  ) {}

  /**
   * Return the Map.
   *
   * @return \Drupal\simple_interactive_maps\InteractiveMapInterface
   *   Map being built.
   */
  public function getMap(): InteractiveMapInterface {
    return $this->interactiveMap;
  }

  /**
   * Return the current render data build.
   *
   * @return \Drupal\simple_interactive_maps\MapRenderHelper
   *   Render data.
   */
  public function getRenderData(): MapRenderHelper {
    return $this->renderData;
  }

  /**
   * Set the render data.
   *
   * @param \Drupal\simple_interactive_maps\MapRenderHelper $renderData
   *   Render data.
   */
  public function setRenderData(MapRenderHelper $renderData): void {
    $this->renderData = $renderData;
  }

  /**
   * Return the context.
   *
   * @return string
   *   Context.
   */
  public function getContext(): string {
    return $this->context;
  }

  /**
   * Set the context.
   *
   * @param string $context
   *   Context.
   */
  public function setContent(string $context): void {
    $this->context = $context;
  }

}
