<?php

declare(strict_types=1);

namespace Drupal\simple_interactive_maps;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for map_definition plugins.
 */
abstract class MapDefinitionPluginBase extends PluginBase implements MapDefinitionInterface {

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritDoc}
   */
  abstract public function mapData(): array;

}
