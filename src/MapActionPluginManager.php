<?php

declare(strict_types=1);

namespace Drupal\simple_interactive_maps;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\simple_interactive_maps\Annotation\MapAction;

/**
 * MapAction plugin manager.
 */
final class MapActionPluginManager extends DefaultPluginManager {

  /**
   * Constructs the object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/MapAction', $namespaces, $module_handler, MapActionInterface::class, MapAction::class);
    $this->alterInfo('map_action_info');
    $this->setCacheBackend($cache_backend, 'map_action_plugins');
  }

}
