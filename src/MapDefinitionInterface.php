<?php

declare(strict_types=1);

namespace Drupal\simple_interactive_maps;

/**
 * Interface for map_definition plugins.
 */
interface MapDefinitionInterface {

  /**
   * Returns the translated plugin label.
   */
  public function label(): string;

  /**
   * Returns the array containing the path, label, and id for all map regions.
   *
   * @return array
   *   Map data.
   */
  public function mapData(): array;

}
