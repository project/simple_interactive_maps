<?php

declare(strict_types=1);

namespace Drupal\simple_interactive_maps\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines map_definition annotation object.
 *
 * @Annotation
 */
final class MapDefinition extends Plugin {

  /**
   * The plugin ID.
   */
  public string $id;

  /**
   * The human-readable name of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public string $title;

  /**
   * The description of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public string $description;

  /**
   * Flag to indicate if the map sets up a default tooltip on creation.
   */
  public bool $uses_default_tooltip = FALSE;

  /**
   * The default tooltip text.
   */
  public string $default_tooltip = '';

  /**
   * Category for grouping maps within the UI.
   */
  public string $map_category = '';

}
