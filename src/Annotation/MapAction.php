<?php

declare(strict_types=1);

namespace Drupal\simple_interactive_maps\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines map_action annotation object.
 *
 * @Annotation
 */
final class MapAction extends Plugin {

  /**
   * The plugin ID.
   */
  public string $id;

  /**
   * The human-readable name of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public string $title;

  /**
   * The description of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public string $description;

  /**
   * Flag to make the plugin not visible to users.
   */
  public bool $is_system = FALSE;

}
