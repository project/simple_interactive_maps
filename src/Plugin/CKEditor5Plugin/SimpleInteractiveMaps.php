<?php

namespace Drupal\simple_interactive_maps\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;
use Drupal\Core\Url;
use Drupal\editor\EditorInterface;

/**
 * Defines the "simpleInteractiveMaps" plugin.
 */
class SimpleInteractiveMaps extends CKEditor5PluginDefault {

  /**
   * {@inheritdoc}
   */
  public function getDynamicPluginConfig(array $static_plugin_config, EditorInterface $editor): array {
    $dynamic_plugin_config = $static_plugin_config;

    $dynamic_plugin_config['simpleInteractiveMaps']['dialogURL'] = Url::fromRoute('simple_interactive_maps.editor_dialog')->setRouteParameter('editor', $editor->id())->toString(TRUE)->getGeneratedUrl();

    return $dynamic_plugin_config;
  }

}
