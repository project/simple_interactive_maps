<?php

namespace Drupal\simple_interactive_maps\Plugin\MapAction;

use Drupal\Core\Form\FormStateInterface;
use Drupal\simple_interactive_maps\MapActionPluginBase;

/**
 * Display content in a modal popup.
 *
 * @MapAction (
 *   id = "modal_content",
 *   label = @Translation("Display Modal Content"),
 *   description = @Translation("Display content in a modal popup."),
 *   is_system = FALSE,
 * )
 */
class ModalContentAction extends MapActionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {

    $form['modal_content'] = [
      '#type' => 'text_format',
      '#format' => $this->configuration['modal_content']['format'],
      '#default_value' => $this->configuration['modal_content']['value'],
      '#rows' => 10,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['modal_content'] = $form_state->getValue('modal_content');
  }

  /**
   * {@inheritdoc}
   */
  public function getActionLibrary(): string {
    return 'simple_interactive_maps/modal_content';
  }

  /**
   * {@inheritdoc}
   */
  public function getActionConfiguration(): array {
    $config = $this->getConfiguration();

    return ['modal_content' => check_markup($config['modal_content']['value'], $config['modal_content']['format'])];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'modal_content' => [
        'value' => '',
        'format' => 'basic_html',
      ],
    ];
  }

}
