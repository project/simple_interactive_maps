<?php

namespace Drupal\simple_interactive_maps\Plugin\MapAction;

use Drupal\Core\Form\FormStateInterface;
use Drupal\simple_interactive_maps\MapActionPluginBase;

/**
 * No Action plugin.
 *
 * @MapAction(
 *   id = "none",
 *   label = @Translation("None"),
 *   description = @Translation("No action is taken when the map is clicked."),
 *   is_system = TRUE,
 *   weight = -100
 * )
 */
class NoneAction extends MapActionPluginBase {

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function getActionLibrary(): string {
    return 'simple_interactive_maps/no_action';
  }

  /**
   * {@inheritDoc}
   */
  public function getActionConfiguration(): array {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {

    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {

  }

}
