<?php

declare(strict_types=1);

namespace Drupal\simple_interactive_maps;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\simple_interactive_maps\Annotation\MapDefinition;

/**
 * MapDefinition plugin manager.
 */
final class MapDefinitionPluginManager extends DefaultPluginManager {

  /**
   * Constructs the object.
   *
   * @param \Traversable $namespaces
   *   The namespaces.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/MapDefinition', $namespaces, $module_handler, MapDefinitionInterface::class, MapDefinition::class);
    $this->alterInfo('map_definition_info');
    $this->setCacheBackend($cache_backend, 'map_definition_plugins');
  }

}
