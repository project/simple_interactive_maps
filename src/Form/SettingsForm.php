<?php

declare(strict_types=1);

namespace Drupal\simple_interactive_maps\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Simple Interactive Maps settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'simple_interactive_maps_settings';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['default_fill_color'] = [
      '#default_value' => $this->config('simple_interactive_maps.settings')
        ->get('default_fill_color'),
      '#description' => $this->t('The default fill color for map regions.'),
      '#title' => $this->t('Default fill color'),
      '#type' => 'color',
    ];

    $form['default_stroke_color'] = [
      '#default_value' => $this->config('simple_interactive_maps.settings')
        ->get('default_stroke_color'),
      '#description' => $this->t('The default stroke color for map regions.'),
      '#title' => $this->t('Default stroke color'),
      '#type' => 'color',
    ];

    $form['default_hover_color'] = [
      '#default_value' => $this->config('simple_interactive_maps.settings')
        ->get('default_hover_color'),
      '#description' => $this->t('The default hover color for map regions.'),
      '#title' => $this->t('Default hover color'),
      '#type' => 'color',
    ];

    $form['default_text_color'] = [
      '#default_value' => $this->config('simple_interactive_maps.settings')
        ->get('default_text_color'),
      '#description' => $this->t('The default text color.'),
      '#title' => $this->t('Default text color'),
      '#type' => 'color',
    ];

    $form['default_region_tooltip'] = [
      '#default_value' => $this->config('simple_interactive_maps.settings')
        ->get('default_region_tooltip')['value'] ?? '',
      '#description' => $this->t('The default tooltip for map regions.'),
      '#format' => $this->config('simple_interactive_maps.settings')
        ->get('default_region_tooltip')['format'] ?? 'full_html',
      '#title' => $this->t('Default region tooltip'),
      '#type' => 'text_format',
    ];

    $form['show_shortcodes'] = [
      '#default_value' => $this->config('simple_interactive_maps.settings')
        ->get('show_shortcodes'),
      '#description' => $this->t('Show shortcodes in the map management list for use with CKEditor 4.'),
      '#title' => $this->t('Show shortcodes'),
      '#type' => 'checkbox',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('simple_interactive_maps.settings')
      ->set('default_fill_color', $form_state->getValue('default_fill_color'))
      ->set('default_stroke_color', $form_state->getValue('default_stroke_color'))
      ->set('default_hover_color', $form_state->getValue('default_hover_color'))
      ->set('default_region_tooltip', $form_state->getValue('default_region_tooltip'))
      ->set('default_text_color', $form_state->getValue('default_text_color'))
      ->set('show_shortcodes', $form_state->getValue('show_shortcodes'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array {
    return ['simple_interactive_maps.settings'];
  }

}
