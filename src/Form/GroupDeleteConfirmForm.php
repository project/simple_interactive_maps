<?php

declare(strict_types=1);

namespace Drupal\simple_interactive_maps\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\simple_interactive_maps\Entity\InteractiveMap;
use Drupal\simple_interactive_maps\GroupTableGeneratorTrait;
use Drupal\simple_interactive_maps\InteractiveMapFormTempStoreTrait;
use Drupal\simple_interactive_maps\InteractiveMapInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Provides a confirmation form for deleting a group from a map.
 */
final class GroupDeleteConfirmForm extends ConfirmFormBase {

  use InteractiveMapFormTempStoreTrait, GroupTableGeneratorTrait;

  /**
   * The map that the group will be deleted from.
   *
   * @var \Drupal\simple_interactive_maps\Entity\InteractiveMap|null
   */
  protected ?InteractiveMap $map;

  /**
   * The group to be deleted.
   *
   * @var string|null
   */
  protected ?string $group;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'simple_interactive_maps_group_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure you want to delete this group?');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This will permanently delete the group from this map and cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return new Url('system.admin_config');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, InteractiveMapInterface $interactive_map = NULL, string $group = NULL): array {
    if (!($interactive_map instanceof InteractiveMapInterface)) {
      throw new HttpException(404, 'Interactive map not found.');
    }

    if ($group === NULL) {
      throw new HttpException(404, 'Group not found.');
    }

    $groups = $interactive_map->getGroups();

    if (!array_key_exists($group, $groups)) {
      throw new HttpException(404, 'Group not found in map.');
    }

    $form = parent::buildForm($form, $form_state);

    $form['map'] = [
      '#type' => 'value',
      '#value' => $interactive_map,
    ];

    $form['group'] = [
      '#type' => 'value',
      '#value' => $group,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    /** @var \Drupal\simple_interactive_maps\InteractiveMapInterface $map */
    $map = $form_state->getValue('map');
    $groups = $map->getGroups();

    unset($groups[$form_state->getValue('group')]);
    $map->setGroups($groups);

    $regions = $map->getRegions();
    foreach ($regions as $regionId => $region) {
      if ($region['group'] === $form_state->getValue('group')) {
        $regions[$regionId]['group'] = '';
      }
    }
    $map->setRegions($regions);

    $map->save();
  }

}
