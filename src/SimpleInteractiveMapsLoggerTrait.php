<?php

namespace Drupal\simple_interactive_maps;

use Drupal\Core\Logger\LoggerChannelInterface;

/**
 * Provides a trait for logging.
 */
trait SimpleInteractiveMapsLoggerTrait {

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $loggerChannel;

  /**
   * Get the logger channel.
   *
   * @return \Drupal\Core\Logger\LoggerChannelInterface
   *   The logger channel.
   */
  protected function getMapsLogger(): LoggerChannelInterface {
    if (!isset($this->loggerChannel)) {
      $this->loggerChannel = \Drupal::service('logger.channel.simple_interactive_maps');
    }
    return $this->loggerChannel;
  }

}
