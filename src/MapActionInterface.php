<?php

declare(strict_types=1);

namespace Drupal\simple_interactive_maps;

/**
 * Interface for map_action plugins.
 */
interface MapActionInterface {

  /**
   * Returns the translated plugin label.
   */
  public function label(): string;

  /**
   * Returns the is_system annotation value.
   *
   * @return bool
   *   Is system.
   */
  public function isSystem(): bool;

  /**
   * Return the drupal library for this action.
   *
   * @return string
   *   Library name.
   */
  public function getActionLibrary(): string;

  /**
   * Processes the configuration for use in the javascript client.
   *
   * @return array
   *   Processed configuration.
   */
  public function getActionConfiguration(): array;

}
